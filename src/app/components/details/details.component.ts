import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  product: any = {
    id: '',
    name: '',
    code: '',
    price: ''
  };
  constructor(
    public productService: ProductService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.params.subscribe((params: Params) => {
      // console.log(params.id);
      params.id;
      this.productService.getProduct(params.id)
        .subscribe(
          res => {
            this.product = res;
          });
    });
  }

  ngOnInit() {
  }

  onDeleteClick(id) {
    this.productService.deleteProduct(id).subscribe(
      next => {
        this.router.navigate(['/']);
      },
      error => {
        console.log(error);
      },
      () => {
        console.log('Data Deleted');
      }
    );
  }

  onEditClick(id) {
    console.log(id);
    this.productService.getProduct(id).subscribe(
      next => {
        this.router.navigate(['/product/' + id + '/edit']);
      },
      error => {
        console.log(error);
      },
      () => {
        console.log('success');
      }
    )
  }
}
