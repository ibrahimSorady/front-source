import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  public products: any = [];
  product:any = {
      id: '',
      name: '',
      price: ''
  };
  constructor(public productService: ProductService) { }

  ngOnInit() {
    this.productService.getProducts()
            .subscribe(
                next => {
                    this.products = next;
                    console.log(next[0]);
                    
                },
                error => {
                    console.log(error);
                },
                () => {
                    console.log('Data Compleated');
                }
            );
  }

}
