import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  public products: any = [];
  product:any = {
      id: '',
      name: '',
      price: ''
  };
  constructor(
    public productService:ProductService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
  }
  onSubmit() {
        this.productService.createProduct(this.product)
        .subscribe(
            next => {
                this.product = next;
                this.router.navigate(['/product/'+this.product.id]);
            },
            error => {
                console.log(error);
            },
            () => {
                console.log('Data Created');
            }
        );
       
    
}

  
}
