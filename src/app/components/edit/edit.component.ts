import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  product: any = {
    id: '',
    name: '',
    code: '',
    price: ''
  };
  constructor(
    public productService: ProductService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.params.subscribe((params: Params) => {
      this.productService.getProduct(params.id).subscribe(
        next => {
          this.product = next;
        },
        error => {
          console.log(error);
        },
        () => {
          console.log('Data Loaded');
        });
    });
  }

  ngOnInit() {
  }

  onUapdate(){
    this.productService.updeteProduct(this.product).subscribe(
      next => {
        this.product = next;
        this.router.navigate(['/product/'+this.product.id]);
        console.log(next);
      },
      error => {
        console.log(error);
      },
      () => {
        console.log('Data Updated');
      });
  }
}
