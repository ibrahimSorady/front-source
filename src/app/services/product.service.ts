import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Headers} from '@angular/http';
import { RequestOptions } from '@angular/http';
import { IProduct } from '../product';
import { Observable } from 'rxjs';
// import 'rxjs/add/operator/map';

let headers;
let options;
// let token;
// let user_headers;
// let user_option;

@Injectable()
export class ProductService {
    private base_url = 'http://127.0.0.1:8000/api/';
    constructor(public http: HttpClient) {
        headers = new Headers({ 'Content-Type': 'application/json' });
        options = new RequestOptions({ headers: headers });
    }
    getProducts(): Observable<IProduct[]> {
        return this.http.get<IProduct[]>(this.base_url + 'products');
    }

    getProduct(id): Observable<IProduct[]> {
        return this.http.get<IProduct[]>(this.base_url + 'product/' + id);
    }
    createProduct(product) {
        return this.http.post(this.base_url + 'product/create', product, options);
    }
    deleteProduct(id) {
        return this.http.delete(this.base_url + 'product/' + id);
    }
    updeteProduct(product) {
        return this.http.put(this.base_url + 'product/' + product.id, product);
    }
}
